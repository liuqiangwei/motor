/*=======================================================
2014.02.21    超声波测距1602显示
							+ 蓝牙发送绘图

LCD1602部分：DB与P1口正对应;RW = P2^5,RS = P2^6,E = P2^7
HC-SR04部分：Trig = P0^0,Echo = P0^1

question:TWO main function,how to control them work at a time


========================================================*/
#include<reg51.h>
#include<intrins.h>
#define uint unsigned int
#define uchar unsigned char

sbit RS = P3^7;
sbit RW = P3^6;
sbit E  = P3^5;

sbit Trig = P1^0;
sbit Echo = P1^1;

uint time,distance;
uchar g,s,b,i,tl,th;
uchar code table1[]={"Distance:"};
uchar code table2[]={"Make By LTREE!"};
uchar code table[]={0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};

void distance_test();
void LCD_writeCMD(uchar cmd);
void LCD_writeDATA(uchar data0);
void delay_20us();
void UARTinit();
void DataSend(void);

void delay5ms()   //   5ms  3us
{
	uint i;
	for(i = 555;i > 0;i--){_nop_();}
}
void Echo_init()
{
	Trig = 0;
	Echo = 0;
	TMOD = 0X21;
	TH0 = 0;
	TL0 = 0;
	ET0 = 1;
	EA = 1;
}
void LCD_init()
{	
	LCD_writeCMD(0X38);
	LCD_writeCMD(0X0c);
	LCD_writeCMD(0X06);
	LCD_writeCMD(0X01);
	LCD_writeCMD(0X80);
	for(i=0;i<9;i++)
	{LCD_writeDATA(table1[i]);}
	LCD_writeCMD(0Xc2);
	for(i=0;i<14;i++)
	{LCD_writeDATA(table2[i]);}
}
void LCD_writeCMD(uchar cmd)
{
	E = 0;RS = 0;RW = 0;
	P2 = cmd;_nop_();
	E = 1;delay5ms();E = 0;
}
void LCD_writeDATA(uchar data0)
{
	E = 0;RS = 1;RW = 0;
	P2 = data0;_nop_();
	E = 1;delay5ms();E = 0;
}

void UARTinit()
{
	SCON = 0X50;
	PCON = 0X80;
	TMOD = 0X21;
	TH1 = 0XFA;
	TL1 = 0XFA;
	TR1 = 1;
	ES = 0;
}

void main()
{
	P2 = 0X00;
	LCD_init();
	UARTinit();
	while(1)
	{
		distance_test();	
			
		LCD_writeCMD(0x8a);
		LCD_writeDATA(table[b]);
		LCD_writeCMD(0x8b);
		LCD_writeDATA(table[s]);
		LCD_writeCMD(0x8c);
		LCD_writeDATA(table[g]);

		DataSend();
	}
}

void DataSend(void)
{
	SBUF = 0xAB;		
	while(!TI);
	TI = 0;		
	
	SBUF = b;		
	while(!TI);
	TI = 0;

	SBUF = s;		
	while(!TI);
	TI = 0;

	SBUF = g;		
	while(!TI);
	TI = 0;
		
}
void distance_test()
{
	Echo_init();
	Trig = 1;
	delay_20us();
	Trig = 0;	
	while(Echo == 0);
	TR0 = 1;
	while(Echo == 1);
	TR0 = 0;
	th = TH0;tl = TL0;
	time = th*256 + tl;
	distance = (uint)(time*0.017);

	b = distance/100;
	s = distance%100/10;
	g = distance%10;
	
	delay5ms();delay5ms();delay5ms();delay5ms();
	delay5ms();delay5ms();delay5ms();delay5ms();

}

void delay_20us()
 {  _nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	  _nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
 }