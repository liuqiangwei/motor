/*==================================================
2014.02.20 飞思卡尔前避障小车测试制作

in1:1 in2:0		GO FORWARD
in3:1 in4:0   GO FORWARD          正常M1快一些
====================================================*/
#include<reg51.h>
#include<intrins.h>
#define uint unsigned int
#define uchar unsigned char
sbit RS = P2^7;
sbit RW = P2^6;
sbit E  = P2^5;
sbit Trig = P2^1;
sbit Echo = P2^0;
uint time;
uchar g,s,b,distance,th,tl,i,j,timer;
uchar code table1[]={"Distance:"};
uchar code table2[]={"Make By LTREE!"};
uchar code table[]={0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};
sbit in = P3^2;
//sbit duoji= P0^2;
sbit in1  = P3^7;
sbit in2  = P3^6;
sbit in3  = P3^5;
sbit in4  = P3^4;
sbit KG   = P2^2;
void init();
void Echo_init()；
void LCD_init();
void delay5ms();
void delayms(uint n);
void delay_20us();
void PWM_ts();
void distance_test();
void LCD_writeCMD(uchar cmd);
void LCD_writeDATA(uchar data0);
void delay5ms()   //   5ms  3us
{
	uint i;
	for(i = 555;i > 0;i--){_nop_();}
}
void delayms(uint n)
{
	uint i;
	for(i = n;i > 0;i--)
	{_nop_();_nop_();_nop_();_nop_();_nop_();
	 _nop_();_nop_();_nop_();_nop_();_nop_();}
}
void delay_20us()
 {  _nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	  _nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
 }
void init()
{	
	TMOD = 0X01;
//	TH1 = 0XFE;
//	TL1 = 0X0C;
	EA = 1;
//	ET1 = 1;
//	TR1 = 1;
	EX0 = 1;
	IT0 = 1;

	in1=1;in2=0;in3=1;in4=0;
}
void Echo_init()
{
	Trig = 0;
	Echo = 0;
	TMOD = 0X01;
	TH0 = 0;
	TL0 = 0;
	EA = 1;
	ET0 = 1;	
}
void main()
{
	init();
	LCD_init();
	while(1)
	{
	distance_test();	
	LCD_writeCMD(0x8a);
	LCD_writeDATA(table[b]);
	LCD_writeCMD(0x8b);
	LCD_writeDATA(table[s]);
	LCD_writeCMD(0x8c);
	LCD_writeDATA(table[g]);
	}
}

void LCD_init()
{	
	LCD_writeCMD(0X38);
	LCD_writeCMD(0X0c);
	LCD_writeCMD(0X06);
	LCD_writeCMD(0X01);
	LCD_writeCMD(0X80);
	for(i=0;i<9;i++)
	{LCD_writeDATA(table1[i]);}
	LCD_writeCMD(0Xc2);
	for(i=0;i<14;i++)
	{LCD_writeDATA(table2[i]);}
}
void LCD_writeCMD(uchar cmd)
{
	E = 0;RS = 0;RW = 0;
	P1 = cmd;_nop_();
	E = 1;delay5ms();E = 0;
}
void LCD_writeDATA(uchar data0)
{
	E = 0;RS = 1;RW = 0;
	P1 = data0;_nop_();
	E = 1;delay5ms();E = 0;
}
/*void PWM_ts()
{
	if(timer > 100) {timer = 0;}
	if(timer < 40)  
	{	in3 = 1;
		if(timer < 30)
			 {in1 = 1;}
		else in1 = 0;
	}
	else {in1 = 0;in3 = 0;}
}*/
void distance_test()
{
	Echo_init()
	Trig = 1;
	delay_20us();
	Trig = 0;	
	while(Echo == 0);
	TR0 = 1;
	while(Echo == 1);
	TR0 = 0;
	th = TH0;tl = TL0;
	time = th*256 + tl;
	distance = time*0.017;
	if(distance < 25)
	{in1 = 0;in3 = 0;}
	b = distance/100;
	s = distance%100/10;
	g = distance%10;
}
void timer0() interrupt 1
{}
void exter0() interrupt 0
{
	uchar XTM1,XTM2,IRcode1,IRcode2;
	EX0 = 0;
	if(!in)
	{
		delayms(287);
		if(!in)
		{
			while(!in);delayms(118);
			if(in)
			{while(in);
			for(j=0;j<8;j++)
			{
				while(!in);delayms(38);
				XTM1 = XTM1 >> 1;
				if(in)
				{
					XTM1 = XTM1 | 0X80;
					while(in);
				}
			}//
			for(j=0;j<8;j++)
			{
				while(!in);delayms(38);
				XTM2 = XTM2 >> 1;
				if(in)
				{
					XTM2 = XTM2 | 0X80;
					while(in);
				}
			}//
			for(j=0;j<8;j++)
			{
				while(!in);delayms(38);
				IRcode1 = IRcode1 >> 1;
				if(in)
				{
					IRcode1 = IRcode1 | 0X80;
					while(in);
				}
			}//
			for(j=0;j<8;j++)
			{
				while(!in);delayms(38);
				IRcode2 = IRcode2 >> 1;
				if(in)
				{
					IRcode2 = IRcode2 | 0X80;
					while(in);
				}
			}//
			}else goto endchk;
		}else goto endchk;
	}else goto endchk;
endchk:
{
	if(IRcode1 == 0x44)
	{	
		KG = 0;
	}
	if(IRcode1 == 0x45)
	{
		KG = 1;
	}
	if(IRcode1 == 0x40)
	{
		in1 = 1;in3 = 1;
	}
	if(IRcode1 == 0x46)
	{
		in1 = 0;in3 = 0;
	}
}
	EX0 = 1;
}

/*void PWM() interrupt 3
{
	TH1 = 0XFE;
	TL1 = 0X0C;
	timer++;
}*/